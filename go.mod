module gitlab.com/connorbrez/go-identity-server

go 1.22.0

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/handlers v1.5.2
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.2
	github.com/mailgun/mailgun-go/v3 v3.6.4
	github.com/pkg/errors v0.9.1
	github.com/pressly/goose v2.7.0+incompatible
	github.com/rs/cors v1.8.2
	github.com/satori/go.uuid v1.2.0
	gitlab.com/connorbrez/gotils v0.1.0
	golang.org/x/crypto v0.23.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
)

require (
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/go-chi/chi v4.0.0+incompatible // indirect
	github.com/go-jose/go-jose/v3 v3.0.3
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
