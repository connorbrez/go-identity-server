package main

import (
	"fmt"
    "github.com/gorilla/securecookie"
    "gitlab.com/connorbrez/go-identity-server/middleware"
    "log"
	"net/http"
	"os"
    "path/filepath"

    _ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/connorbrez/go-identity-server/controllers"
	"gitlab.com/connorbrez/go-identity-server/services"
	"gitlab.com/connorbrez/go-identity-server/util"

	"gitlab.com/connorbrez/gotils"
)

type ContextKey string

const ContextUserKey ContextKey = "user"

func main() {
	var env = os.Getenv("ENV")
	if env == "" {
		env = "local"
	}
	log.Printf("Running in %s mode... \n", env)


	folder, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	err = os.Setenv("IDENTITY_ABS_PATH", folder)
	if err != nil {
		log.Fatal(err)
	}

	config := util.LoadConfig(env)
	privateKey, err := gotils.LoadRSAPrivateKeyFromPath(config.SecretPath)
	if err != nil {
		log.Fatalf("Failed to load private key from path...\n%s\n", err)
	}


	s := securecookie.New([]byte(config.Http["hash_key"]), []byte(config.Http["block_key"]))


	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", config.Database["user"], config.Database["pass"], config.Database["host"], config.Database["port"], config.Database["database"])
	db := util.InitDB(config.Database["type"], dsn)

	err = util.DoMigrations(config.Database["type"], "migrations/", db)
	if err != nil {
		log.Fatal(err)
	}

	// service init
	var emailService = services.EmailService{Config: config.Mail}
	var roleService = services.RoleService{DB: db}
	var permissionService = services.PermissionService{DB: db}
	var authService = services.AuthService{PrivateKey: privateKey, Jwt: config.Jwt}
	var userService = services.UserService{DB: db, EmailService: &emailService, AppDomain: config.AppDomain, AuthService: &authService}

	var authMW = middleware.AuthenticationMiddleware{AuthService: &authService, Cookie:s, UserService: &userService}

	// controller init
	var userController = controllers.UserController{UserService: &userService, AuthService: &authService, RoleService: &roleService, PermissionService: &permissionService, AuthMW: &authMW}
	var roleController = controllers.RoleController{RoleService: &roleService, AuthMW: &authMW}
	var permissionController = controllers.PermissionController{PermissionService: &permissionService, AuthService: &authService, AuthMW: &authMW}
	var authController = controllers.AuthController{AuthService: &authService, UserService: &userService, Cookie: s, AppDomain: config.AppDomain}

	router := mux.NewRouter()

	router.PathPrefix("/roles").Handler(roleController.Routes())
	router.PathPrefix("/permissions").Handler(permissionController.Routes())
	router.PathPrefix("/users").Handler(userController.Routes())
	router.PathPrefix("/auth").Handler(authController.Routes())

	port := os.Getenv("PORT")
	loggingHandler := handlers.CustomLoggingHandler(os.Stdout, router, gotils.JSONLoggingWriter)
	corsHandler := cors.New(
		cors.Options{
			AllowedOrigins: []string{
				fmt.Sprintf("%v", config.AppDomain),
			},

			AllowCredentials: true,
		}).Handler(loggingHandler)
	if env == "local" {
		port = config.Http["port"]
	}

	log.Printf("Starting HTTP server with App Domain: %v... Port: %v... \n", config.AppDomain, port)
	err = http.ListenAndServe(fmt.Sprintf(":%s", port), corsHandler)
	if err != nil {
		log.Fatalf("HTTP server failed to start: %s\n", err)
	}
}
