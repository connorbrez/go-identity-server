-- +goose Up

INSERT INTO users SET uid = 'c98caab6-2251-424b-a943-7083c2d16659', name = 'John Smith', password = '$2a$10$kp5Kb8avtJDtFhaxGePPuuXtuHnBIJ/f0DkTDWFi.ScQi1.vsZqJ6', email = 'testing@test.com', active = '1', reset_password = '0';
INSERT INTO user_roles set role_id = '1', user_uid = 'c98caab6-2251-424b-a943-7083c2d16659';



-- +goose Down

DELETE FROM users WHERE uid = 'c98caab6-2251-424b-a943-7083c2d16659';
DELETE FROM user_roles WHERE user_uid = 'c98caab6-2251-424b-a943-7083c2d16659' AND role_id = '1';