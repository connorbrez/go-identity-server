s="local"

while getopts ":s:p:" o; do
    case "${o}" in
        s)
            s=${OPTARG}
            ;;
    esac
done
shift $((OPTIND-1))

echo "rolling ${s}"

rm src/rsa_$s
rm src/rsa_$s.pub
ssh-keygen -t rsa -b 4096 -m PEM -f src/rsa_$s

