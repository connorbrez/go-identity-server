package util

import (
    "database/sql"
    "encoding/json"
    "github.com/gorilla/securecookie"
    "log"
    "net/http"
    "os"
    "time"

    "github.com/pressly/goose"
    "gitlab.com/connorbrez/go-identity-server/models"
)

func LoadConfig(env string) models.Config {
	var file = "config/" + env + ".json"
	log.Printf("Loading config from: %s", file)
	configFile, err := os.Open(file)
	if err != nil {
		log.Printf("Could not open config file: %s", file)
		file = "../config/" + env + ".json"
		log.Printf("Loading config from: %s", file)
		configFile, err = os.Open(file)
		if err != nil {
			log.Printf("Could not open config file: %s", file)
		}
	}
	defer configFile.Close()

	var config models.Config
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		log.Printf("Could not decode config file: %s", file)
		log.Fatal(err.Error())
	}

	return config
}

func InitDB(t, dsn string) *sql.DB {
	db, err := sql.Open(t, dsn)
	if err != nil {
		log.Fatalf("Failed to connect to database...\n%s\n", err.Error())
	}

	if err = db.Ping(); err != nil {
		log.Fatalf("Failed to ping database...\n%s\n", err.Error())
	}

	db.SetConnMaxLifetime(time.Second)
	return db
}

func DoMigrations(t, dir string, db *sql.DB) error {
	err := goose.SetDialect(t)
	if err != nil {
		return err
	}
	goose.SetTableName("goose_db_version")
	return goose.Run("up", db, dir)
}

func DoMigrationsTest(t, dir string, db *sql.DB) error {
	err := goose.SetDialect(t)
	if err != nil {
		return err
	}
	goose.SetTableName("goose_db_version_test")
	return goose.Run("up", db, dir)
}

func  GetTokenFromCookie(c *securecookie.SecureCookie,  req *http.Request) (string, error) {
	cookie, err := req.Cookie("identity_token")
	if err != nil {
		return "", err
	}

	var token string
	err = c.Decode("identity_token", cookie.Value, &token)
	if err != nil {
		return "", err
	}


	return token, nil
}
