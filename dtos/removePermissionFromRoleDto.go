package dtos

type RemovePermissionFromRoleDto struct {
	RoleID       int `json:"role_id"`
	PermissionID int `json:"permission_id"`
}
