package dtos

import uuid "github.com/satori/go.uuid"

type RevokeRoleDto struct {
	RoleID  int       `json:"role_id"`
	UserUID uuid.UUID `json:"user_uid"`
}
