package dtos

import uuid "github.com/satori/go.uuid"

type ActivateUserDto struct {
	UserUID uuid.UUID `json:"user_uid"`
	Code    string    `json:"code"`
}
