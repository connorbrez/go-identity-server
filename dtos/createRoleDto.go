package dtos

type CreateRoleDto struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}
