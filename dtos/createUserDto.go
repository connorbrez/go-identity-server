package dtos

type CreateUserDto struct {
	Name          string `json:"name"`
	Email         string `json:"email"`
	Password      string `json:"password"`
	ResetPassword bool   `json:"reset_password"`
}
