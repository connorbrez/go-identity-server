package dtos

import (
	uuid "github.com/satori/go.uuid"
)

type UserInfoDto struct {
	UID           uuid.UUID `json:"uid"`
	Name          string    `json:"name"`
	Email         string    `json:"email"`
	Active        bool      `json:"active"`
	ResetPassword bool      `json:"reset_password"`
	Token         string    `json:"token"`
}
