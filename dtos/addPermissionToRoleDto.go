package dtos

type AddPermissionToRoleDto struct {
	RoleID       int `json:"role_id"`
	PermissionID int `json:"permission_id"`
}
