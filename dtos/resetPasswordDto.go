package dtos

import (
	uuid "github.com/satori/go.uuid"
)

type PasswordResetDto struct {
	UserUID uuid.UUID `json:"user_uid"`
	Old     string    `json:"old_password"`
	New     string    `json:"new_password"`
}
