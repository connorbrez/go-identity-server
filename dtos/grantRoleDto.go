package dtos

import uuid "github.com/satori/go.uuid"

type GrantRoleDto struct {
	RoleID  int       `json:"role_id"`
	UserUID uuid.UUID `json:"user_uid"`
}
