FROM golang:1.22 as builder
WORKDIR /go/src/gitlab.com/connorbrez/go-identity-server
COPY . .

ENV CGO_ENABLED 0
ENV ENV docker
RUN GOOS=linux ./get.sh && go build -o identity-server ./src/

FROM golang:1.22
EXPOSE 8899
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/connorbrez/go-identity-server/src/config config/
COPY --from=builder /go/src/gitlab.com/connorbrez/go-identity-server/src/migrations migrations/
COPY --from=builder /go/src/gitlab.com/connorbrez/go-identity-server/src/rsa_prod rsa_prod
COPY --from=builder /go/src/gitlab.com/connorbrez/go-identity-server/src/rsa_prod.pub rsa_prod.pub
COPY --from=builder /go/src/gitlab.com/connorbrez/go-identity-server/identity-server identity-server
ENV ENV docker
CMD ["/app/identity-server"]
