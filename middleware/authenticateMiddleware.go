package middleware

import (
	"context"
    "github.com/gorilla/securecookie"
    "gitlab.com/connorbrez/go-identity-server/util"
    "log"
	"net/http"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/connorbrez/go-identity-server/enums"
	"gitlab.com/connorbrez/go-identity-server/services"
	"gitlab.com/connorbrez/gotils"
)

type ContextKey string

const ContextUserKey ContextKey = "user"

type AuthenticationMiddleware struct {
	AuthService *services.AuthService
	UserService *services.UserService
	Cookie *securecookie.SecureCookie
}

func (amw *AuthenticationMiddleware) HasPermission(next http.HandlerFunc, permId enums.Permission) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {

		jwt, err := util.GetTokenFromCookie(amw.Cookie, r)
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		claims, err := amw.AuthService.ValidateJWT(jwt)
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		uid, err := uuid.FromString(claims.Subject)
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		v, err := amw.UserService.HasPermission(uid.String(), int(permId))
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		if v {
			ctx := context.WithValue(r.Context(), ContextUserKey, uid)
			next.ServeHTTP(rw, r.WithContext(ctx))
			return
		}

		gotils.WriteJSON("unauthorized", http.StatusUnauthorized, rw)
	})
}

func (amw *AuthenticationMiddleware) HasValidCookie(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {

		jwt, err := util.GetTokenFromCookie(amw.Cookie, r)
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		claims, err := amw.AuthService.ValidateJWT(jwt)
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		uid, err := uuid.FromString(claims.Subject)
		if err != nil {
			log.Println(err)
			gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
			return
		}

		ctx := context.WithValue(r.Context(), ContextUserKey, uid)
		next.ServeHTTP(rw, r.WithContext(ctx))
		return
	})
}


