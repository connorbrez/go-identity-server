package models

type Config struct {
	SecretPath string            `json:"secret_path"`
	AppURL     string            `json:"app_url"`
	AppDomain  string            `json:"app_domain"`
	Database   map[string]string `json:"database"`
	Http       map[string]string `json:"http"`
	Jwt        map[string]string `json:"jwt"`
	Mail       map[string]string `json:"mail"`
}
