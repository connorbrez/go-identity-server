package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type UserPermission struct {
	ID        int       `json:"id"`
	PermID    int       `json:"permission_id"`
	UserUID   uuid.UUID `json:"user_uid"`
	CreatedOn time.Time `json:"created_on"`
	CreatedBy uuid.UUID `json:"created_by"`
}
