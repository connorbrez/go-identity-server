package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/connorbrez/go-identity-server/dtos"
)

type User struct {
	UID           uuid.UUID `json:"uid"`
	Name          string    `json:"name"`
	Email         string    `json:"email"`
	Password      string    `json:"password"`
	Active        bool      `json:"active"`
	ResetPassword bool      `json:"reset_password"`
	CreatedOn     time.Time `json:"created_on"`
	UpdatedOn     time.Time `json:"updated_on"`
	CreatedBy     uuid.UUID `json:"created_by"`
	UpdatedBy     uuid.UUID `json:"updated_by"`
}

func (u *User) ToUserInfo() dtos.UserInfoDto {
	return dtos.UserInfoDto{UID: u.UID, Name: u.Name, Email: u.Email, Active: u.Active, ResetPassword: u.ResetPassword}
}
