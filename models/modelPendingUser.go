package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type PendingUser struct {
	ID        int       `json:"id"`
	UserUID   uuid.UUID `json:"user_uid"`
	Code      string    `json:"code"`
	CreatedOn time.Time `json:"created_on"`
	UpdatedOn time.Time `json:"updated_on"`
	CreatedBy uuid.UUID `json:"created_by"`
	UpdatedBy uuid.UUID `json:"updated_by"`
}
