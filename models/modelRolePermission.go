package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type RolePermission struct {
	ID        int       `json:"id"`
	PermID    int       `json:"permission_id"`
	RoleID    int       `json:"role_id"`
	CreatedOn time.Time `json:"created_on"`
	CreatedBy uuid.UUID `json:"created_by"`
}
