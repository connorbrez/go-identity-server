package controllers

import (
    "github.com/gorilla/securecookie"
    "gitlab.com/connorbrez/go-identity-server/util"
    "log"
	"net/http"
    "time"

    "github.com/gorilla/mux"
	"gitlab.com/connorbrez/go-identity-server/dtos"
	"gitlab.com/connorbrez/go-identity-server/services"
	"gitlab.com/connorbrez/gotils"
)

// AuthController handles HTTP requests related to authentication operations.
type AuthController struct {
	AuthService *services.AuthService // Service for authentication-related operations
	UserService *services.UserService      // Service for user-related operations
	Cookie      *securecookie.SecureCookie
	AppDomain   string

}

// Routes initializes the routes related to authentication operations.
func (c *AuthController) Routes() http.Handler {
	router := mux.NewRouter().PathPrefix("/auth").Subrouter()
	router.HandleFunc("/valid", c.ValidateToken).Methods(http.MethodGet)
	router.HandleFunc("/login", c.Login).Methods(http.MethodPost)
	return router
}

// Login handles the HTTP POST request to authenticate a user.
// It validates the user's password and returns a signed JWT with a default 30-minute expiry.
func (c *AuthController) Login(rw http.ResponseWriter, r *http.Request) {
	log.Println("login request received...")
	var dto dtos.UserLoginDto
	err := gotils.ReadBody(r, &dto)
	if err != nil {
		log.Println("authController/Login: error parsing body, most likely client error.")
		log.Println(err)
		gotils.WriteJSON("error parsing dto, please check your request for errors.", http.StatusBadRequest, rw)
		return
	}

	identity, err := c.UserService.Login(dto.Email, dto.Password)
	if err != nil {
		log.Println("authController/Login: login validation failed. Client unauthorized.")
		log.Println(err)
		gotils.WriteJSON("unauthorized.", http.StatusUnauthorized, rw)
		return
	}
	log.Println("login request successful.")


	if encoded, err := c.Cookie.Encode("identity_token", identity.Token); err == nil {
		expiry := time.Now().Add(time.Duration(30 * time.Minute))
		cookie := http.Cookie{Name: "identity_token", Domain: c.AppDomain, Value: encoded, Path: "/", Expires: expiry, Secure: true}
		http.SetCookie(rw, &cookie)
		gotils.WriteJSON("OK", http.StatusOK, rw)
		return
	}
	gotils.WriteError(err, http.StatusUnauthorized, rw)
}

// ValidateToken handles the HTTP POST request to validate a JWT token.
// It reads the token from the request body and interacts with AuthService to validate the token.
func (c *AuthController) ValidateToken(rw http.ResponseWriter, r *http.Request) {
	jwt, err := util.GetTokenFromCookie(c.Cookie, r)
	if err != nil {
		log.Println(err)
		gotils.WriteJSON(false, http.StatusOK, rw)
		return
	}

	log.Println("token validation request...")
	_, err = c.AuthService.ValidateJWT(jwt)
	if err != nil {
		log.Println("invalid token.")
		log.Println(err)
		gotils.WriteJSON(false, http.StatusOK, rw)
		return
	}
	log.Println("token valid.")
	gotils.WriteJSON(true, http.StatusOK, rw)
}


