package controllers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/connorbrez/go-identity-server/dtos"
	"gitlab.com/connorbrez/go-identity-server/enums"
	"gitlab.com/connorbrez/go-identity-server/middleware"
	"gitlab.com/connorbrez/go-identity-server/services"
	"gitlab.com/connorbrez/gotils"
)

// PermissionController handles HTTP requests related to permission operations.
type PermissionController struct {
	PermissionService *services.PermissionService // Service for permission-related operations
	AuthService       *services.AuthService       // Service for authentication-related operations
	AuthMW            *middleware.AuthenticationMiddleware
}

// Routes initializes the routes related to permission operations and sets up necessary middlewares.
func (c *PermissionController) Routes() http.Handler {

	router := mux.NewRouter().PathPrefix("/permissions").Subrouter()
	router.HandleFunc("/create", c.AuthMW.HasPermission(c.Create, enums.CreatePermission)).Methods(http.MethodPost)
	router.HandleFunc("/{id}/delete", c.AuthMW.HasPermission(c.Delete, enums.DeletePermission)).Methods(http.MethodDelete)
	router.HandleFunc("/{id}/find", c.AuthMW.HasPermission(c.Find, enums.ReadPermission)).Methods(http.MethodGet)
	router.HandleFunc("", c.AuthMW.HasPermission(c.List, enums.ReadPermission)).Methods(http.MethodGet)

	return router
}

// Create handles the HTTP POST request to create a new permission.
// It reads the request body to get permission details and interacts with PermissionService to create the permission.
func (c *PermissionController) Create(rw http.ResponseWriter, r *http.Request) {
	var newPermission dtos.CreatePermissionDto
	err := gotils.ReadBody(r, &newPermission)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	err = c.PermissionService.Create(newPermission.Name, newPermission.Description)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	gotils.WriteJSON("Permission Created", http.StatusCreated, rw)
}

// Delete handles the HTTP DELETE request to delete a permission by its ID.
// It reads the permission ID from the request URL and interacts with PermissionService to delete the permission.
func (c *PermissionController) Delete(rw http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	err = c.PermissionService.Delete(id)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}

	gotils.WriteJSON("Permission Deleted", http.StatusOK, rw)
}

// Find handles the HTTP GET request to retrieve a permission by its ID.
// It reads the permission ID from the request URL and interacts with PermissionService to find and return the permission.
func (c *PermissionController) Find(rw http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	permission, err := c.PermissionService.Find(id)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}

	gotils.WriteJSON(permission, http.StatusOK, rw)
}

// List handles the HTTP GET request to retrieve all permissions.
// It interacts with PermissionService to list and return all permissions.
func (c *PermissionController) List(rw http.ResponseWriter, r *http.Request) {
	permissions, err := c.PermissionService.List()
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}
	gotils.WriteJSON(permissions, http.StatusOK, rw)
}