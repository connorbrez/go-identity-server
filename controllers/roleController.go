package controllers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/connorbrez/go-identity-server/dtos"
	"gitlab.com/connorbrez/go-identity-server/enums"
	"gitlab.com/connorbrez/go-identity-server/middleware"
	"gitlab.com/connorbrez/go-identity-server/services"
	"gitlab.com/connorbrez/gotils"
)

// RoleController handles HTTP requests related to role operations.
type RoleController struct {
	RoleService *services.RoleService // Service for role-related operations
	AuthService *services.AuthService // Service for authentication-related operations
	AuthMW *middleware.AuthenticationMiddleware
}

// Routes initializes the routes related to role operations and sets up necessary middlewares.
func (c *RoleController) Routes() http.Handler {

	router := mux.NewRouter().PathPrefix("/roles").Subrouter()
	router.HandleFunc("/create", c.AuthMW.HasPermission(c.Create, enums.CreateRole)).Methods(http.MethodPost)
	router.HandleFunc("/{id}/delete", c.AuthMW.HasPermission(c.Delete, enums.DeleteRole)).Methods(http.MethodDelete)
	router.HandleFunc("/{id}/find", c.AuthMW.HasPermission(c.Find, enums.ReadRole)).Methods(http.MethodGet)

	router.HandleFunc("/grant", c.AuthMW.HasPermission(c.Grant, enums.GrantRole)).Methods(http.MethodPost)
	router.HandleFunc("/revoke", c.AuthMW.HasPermission(c.Revoke, enums.RevokeRole)).Methods(http.MethodPost)

	router.HandleFunc("/add", c.AuthMW.HasPermission(c.AddPermission, enums.GrantPermission)).Methods(http.MethodPost)
	router.HandleFunc("/remove", c.AuthMW.HasPermission(c.RemovePermission, enums.RevokePermission)).Methods(http.MethodPost)

	router.HandleFunc("", c.AuthMW.HasPermission(c.List, enums.ReadRole)).Methods(http.MethodGet)

	return router
}

// Create handles the HTTP POST request to create a new role.
// It reads the request body to get role details and interacts with RoleService to create the role.
func (c *RoleController) Create(rw http.ResponseWriter, r *http.Request) {
	var newRole dtos.CreateRoleDto
	err := gotils.ReadBody(r, &newRole)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	err = c.RoleService.Create(newRole.Name, newRole.Description)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	gotils.WriteJSON("Role Created", http.StatusCreated, rw)
}

// Delete handles the HTTP DELETE request to delete a role by its ID.
// It reads the role ID from the request URL and interacts with RoleService to delete the role.
func (c *RoleController) Delete(rw http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	err = c.RoleService.Delete(id)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}

	gotils.WriteJSON("Role Deleted", http.StatusOK, rw)
}

// Find handles the HTTP GET request to retrieve a role by its ID.
// It reads the role ID from the request URL and interacts with RoleService to find and return the role.
func (c *RoleController) Find(rw http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	role, err := c.RoleService.Find(id)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}

	gotils.WriteJSON(role, http.StatusOK, rw)
}

// List handles the HTTP GET request to retrieve all roles.
// It interacts with RoleService to list and return all roles.
func (c *RoleController) List(rw http.ResponseWriter, r *http.Request) {
	roles, err := c.RoleService.List()
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}
	gotils.WriteJSON(roles, http.StatusOK, rw)
}

// AddPermission handles the HTTP POST request to add a permission to a role.
// It reads the request body to get the details of the role and permission and interacts with RoleService.
func (c *RoleController) AddPermission(rw http.ResponseWriter, r *http.Request) {
	var dto dtos.AddPermissionToRoleDto
	err := gotils.ReadBody(r, &dto)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}

	err = c.RoleService.GrantPermission(dto.PermissionID, dto.RoleID)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	gotils.WriteJSON("Permission granted to Role", http.StatusCreated, rw)
}

// RemovePermission handles the HTTP POST request to remove a permission from a role.
// It reads the request body to get the details of the role and permission and interacts with RoleService.
func (c *RoleController) RemovePermission(rw http.ResponseWriter, r *http.Request) {
	var dto dtos.RemovePermissionFromRoleDto
	err := gotils.ReadBody(r, &dto)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}

	err = c.RoleService.RevokePermission(dto.PermissionID, dto.RoleID)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	gotils.WriteJSON("Permission revoked from Role", http.StatusCreated, rw)
}

// Grant handles the HTTP POST request to grant a role to a user.
// It reads the request body to get the details of the user and role and interacts with RoleService.
func (c *RoleController) Grant(rw http.ResponseWriter, r *http.Request) {
	var dto dtos.GrantRoleDto
	err := gotils.ReadBody(r, &dto)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}

	err = c.RoleService.GrantRole(dto.UserUID, dto.RoleID)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	gotils.WriteJSON("Role granted to User", http.StatusCreated, rw)

}

// Revoke handles the HTTP POST request to revoke a role from a user.
// It reads the request body to get the details of the user and role and interacts with RoleService.
func (c *RoleController) Revoke(rw http.ResponseWriter, r *http.Request) {
	var dto dtos.RevokeRoleDto
	err := gotils.ReadBody(r, &dto)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}

	err = c.RoleService.RevokeRole(dto.UserUID, dto.RoleID)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
	}
	gotils.WriteJSON("Role revoked from User", http.StatusCreated, rw)
}