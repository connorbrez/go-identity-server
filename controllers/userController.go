package controllers

import (
	"log"
	"net/http"

    "github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"

	"gitlab.com/connorbrez/go-identity-server/dtos"
	"gitlab.com/connorbrez/go-identity-server/enums"
	"gitlab.com/connorbrez/go-identity-server/middleware"
	"gitlab.com/connorbrez/go-identity-server/services"
	"gitlab.com/connorbrez/gotils"
)

// UserController handles HTTP requests related to user operations.
type UserController struct {
	UserService       *services.UserService      // Service for user-related operations
	AuthService       *services.AuthService      // Service for authentication-related operations
	RoleService       *services.RoleService      // Service for role-related operations
	PermissionService *services.PermissionService // Service for permission-related operations
	AuthMW            *middleware.AuthenticationMiddleware
}

// Routes initializes the routes related to user operations and sets up necessary middlewares.
func (c *UserController) Routes() http.Handler {


	router := mux.NewRouter().PathPrefix("/users").Subrouter()
	router.HandleFunc("/{uid}/find", c.AuthMW.HasPermission(c.Find, enums.ReadUser)).Methods(http.MethodGet)
	router.HandleFunc("/{uid}/activate/{code}", c.Activate).Methods(http.MethodPost)
	router.HandleFunc("/list", c.AuthMW.HasPermission(c.List, enums.ReadUser)).Methods(http.MethodGet)
	router.HandleFunc("/register", c.Register).Methods(http.MethodPost)
	router.HandleFunc("/reset", c.ResetPassword).Methods(http.MethodPost)

	return router
}

// Find handles the HTTP GET request to retrieve a user by their UID.
// It requires the ReadUser permission.
func (c *UserController) Find(rw http.ResponseWriter, r *http.Request) {
	uid, err := uuid.FromString(mux.Vars(r)["uid"])
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}
	user, err := c.UserService.Find(uid)
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}

	gotils.WriteJSON(user.ToUserInfo(), http.StatusOK, rw)
}

// List handles the HTTP GET request to retrieve all users.
// It requires the ReadUser permission.
func (c *UserController) List(rw http.ResponseWriter, r *http.Request) {
	users, err := c.UserService.List()
	if err != nil {
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}
	gotils.WriteJSON(users, http.StatusOK, rw)
}

// Register handles the HTTP POST request to create a new user.
// It hashes and salts the user's password using bcrypt.
func (c *UserController) Register(rw http.ResponseWriter, r *http.Request) {
	log.Println("received request to register user...")
	var newUser dtos.CreateUserDto
	err := gotils.ReadBody(r, &newUser)
	if err != nil {
		log.Println("userController.go/Register: error parsing body, probably bad request.")
		log.Println(err)
		gotils.WriteJSON("error parsing dto, please check your request for errors.", http.StatusBadRequest, rw)
		return
	}

	err = c.UserService.CreatePending(newUser.Name, newUser.Email, newUser.Password, newUser.ResetPassword)
	if err != nil {
		log.Println("userController.go/Register: failed creating identity...")
		log.Println(err)
		gotils.WriteJSON("Error registering", http.StatusInternalServerError, rw)
		return
	}
	log.Println("registration successful.")
	gotils.WriteJSON("User Registered", http.StatusCreated, rw)
}

// Activate handles the HTTP PUT request to activate a user using a unique code.
func (c *UserController) Activate(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	uid, err := uuid.FromString(vars["uid"])
	if err != nil {
		log.Println("userController.go/Activate: failed to parse user uuid")
		log.Println(err)
		gotils.WriteError(err, http.StatusBadRequest, rw)
		return
	}

	err = c.UserService.Activate(vars["code"], uid)
	if err != nil {
		log.Println("userController.go/Activate: failed to activate user...")
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}
	gotils.WriteJSON(true, http.StatusOK, rw)
}

// ResetPassword handles the HTTP POST request to reset a user's password.
func (c *UserController) ResetPassword(rw http.ResponseWriter, r *http.Request) {
	log.Println("received request to register user...")
	var resetPassword dtos.PasswordResetDto
	err := gotils.ReadBody(r, &resetPassword)
	if err != nil {
		log.Println("userController.go/Register: error parsing body, most likely client error.")
		log.Println(err)
		gotils.WriteError(err, http.StatusBadRequest, rw)
		return
	}

	err = c.UserService.ResetPassword(resetPassword.UserUID, resetPassword.New)
	if err != nil {
		log.Println("userController.go/ResetPassword: failed resetting password...")
		log.Println(err)
		gotils.WriteError(err, http.StatusInternalServerError, rw)
		return
	}
	log.Println("Password Reset Successful.")
	gotils.WriteJSON("Password Reset Successfully.", http.StatusOK, rw)
}