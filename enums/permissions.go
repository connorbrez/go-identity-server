package enums

type Permission int

const (
	CreatePermission Permission = iota + 1
	DeletePermission
	ReadPermission
	ReadUser
	DeleteUser
	CreateRole
	DeleteRole
	ReadRole
	GrantPermission
	RevokePermission
	GrantRole
	RevokeRole
)
