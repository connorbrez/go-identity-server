package services

import (
	"database/sql"
	"log"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/connorbrez/go-identity-server/models"
)

// RoleService provides methods for managing roles in the database.
type RoleService struct {
	DB *sql.DB // Database connection.
}

// Create creates a new role with the given name and description.
// It wraps the operation in a transaction to ensure consistency.
func (s *RoleService) Create(name, desc string) error {
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "INSERT INTO roles (name, description) VALUES (?,?)"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(name, desc)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("role %v was created successfuly.\n", name)
	return nil
}

// Delete removes a role from the database by its ID.
// It wraps the operation in a transaction to ensure consistency
func (s *RoleService) Delete(id int) error {
	// TODO: delete role relation in user_roles and role_permissions
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "DELETE FROM roles WHERE id = ?"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("role %v was deleted successfully.\n", id)
	return nil
}

// Find retrieves a role by its ID.
// It returns the role or an error if not found.
func (s *RoleService) Find(id int) (models.Role, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM roles WHERE id = ?")
	if err != nil {
		return models.Role{}, err
	}
	defer stmt.Close()
	var role models.Role
	err = stmt.QueryRow(id).Scan(&role.ID, &role.Name, &role.Description, &role.CreatedOn, &role.UpdatedOn, &role.CreatedBy, &role.UpdatedBy)
	if err != nil {
		return models.Role{}, err
	}
	return role, nil
}

// List retrieves all roles from the database.
// It returns a slice of roles or an error if the query fails.
func (s *RoleService) List() ([]models.Role, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM roles")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	var roles []models.Role
	rows, err := stmt.Query()
	for rows.Next() {
		var role models.Role
		rows.Scan(&role.ID, &role.Name, &role.Description, &role.CreatedOn, &role.UpdatedOn, &role.CreatedBy, &role.UpdatedBy)
		if err != nil {
			return nil, err
		}
		roles = append(roles, role)
	}

	return roles, nil
}

// ListByUser retrieves all roles associated with a specific user.
// It returns a slice of user roles or an error if the query fails.
func (s *RoleService) ListByUser(uid uuid.UUID) ([]models.UserRole, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM user_roles WHERE user_uid = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	var roles []models.UserRole
	rows, err := stmt.Query(uid.String())
	for rows.Next() {
		var role models.UserRole
		rows.Scan(&role.ID, &role.RoleID, &role.UserUID, &role.CreatedOn, &role.CreatedBy)
		if err != nil {
			return nil, err
		}
		roles = append(roles, role)
	}

	return roles, nil
}

// GrantRole will grant the specifed role to the specifed user
func (s *RoleService) GrantRole(userUID uuid.UUID, roleID int) error {
	// TODO: check if role has already been granted
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "INSERT INTO user_roles (role_id, user_uid, created_by) VALUES (?,?,?)"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(roleID, userUID, "")
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("role %v was granted to user %v successfuly.\n", roleID, userUID)
	return nil
}

// RevokeRole will revoke the specifed role from the specifed user
func (s *RoleService) RevokeRole(userUID uuid.UUID, roleID int) error {
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "DELETE FROM user_roles WHERE role_id = ? AND user_uid = ?"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(roleID, userUID)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("role %v was revoked from user %v successfuly.\n", roleID, userUID)
	return nil
}

// GrantPermission will grant the specifed permission to the specifed role
func (s *RoleService) GrantPermission(permID, roleID int) error {
	// TODO: check if permission has already been granted
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "INSERT INTO role_permissions (permission_id, role_id, created_by, created_on) VALUES (?,?,?,?)"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(permID, roleID, "", "")
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("permission %v was granted to role %v successfuly.\n", permID, roleID)
	return nil
}

// RevokePermission will revoke the specifed permission from the specifed role
func (s *RoleService) RevokePermission(permID, roleID int) error {
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "DELETE FROM role_permissions WHERE permission_id = ? AND role_id = ?"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(permID, roleID)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("permission %v was revoked from role %v successfuly.\n", permID, roleID)
	return nil
}

// HasPermission checks the specifed role to see if it has the specified permission
func (s *RoleService) HasPermission(permID, roleID int) (bool, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM role_permissions WHERE role_id = ? AND permission_id = ?")
	if err != nil {
		return false, err
	}
	defer stmt.Close()
	var permission models.RolePermission
	err = stmt.QueryRow(roleID, permID).Scan(&permission.ID, &permission.PermID, &permission.RoleID, &permission.CreatedOn, &permission.CreatedBy)
	if err != nil {
		return false, err
	}
	if permission.RoleID == roleID && permission.PermID == permID {
		return true, nil
	}
	return false, nil
}
