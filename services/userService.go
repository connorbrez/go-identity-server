package services

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"fmt"
	"log"
	"os"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"

	errs "github.com/pkg/errors"
	"gitlab.com/connorbrez/go-identity-server/dtos"
	"gitlab.com/connorbrez/go-identity-server/models"
	"gitlab.com/connorbrez/gotils"
)

const BcrpytCost = 10 // Bcrypt cost for hashing passwords

// UserService provides methods for managing users, including authentication and email services.
type UserService struct {
	DB           *sql.DB // Database connection
	EmailService *EmailService // Email sending service
	AppDomain    string // Application domain name for generating URLs
	AuthService  *AuthService // Authentication service for generating JWTs
}

// HasPermission checks the specifed user to see if they have the specified permission
func (s *UserService) HasPermission(userUID string, permID int) (bool, error) {
	query := `SELECT * FROM role_permissions
              WHERE EXISTS
              (SELECT * FROM user_roles WHERE user_roles.role_id = role_permissions.role_id AND user_roles.user_uid = ?)
              HAVING permission_id = ?`
	stmt, err := s.DB.Prepare(query)
	if err != nil {
		return false, err
	}
	defer stmt.Close()
	var permission models.RolePermission
	err = stmt.QueryRow(userUID, permID).Scan(&permission.ID, &permission.PermID, &permission.RoleID, &permission.CreatedOn, &permission.CreatedBy)
	if err != nil {
		return false, err
	}
	return permID == permission.PermID, nil
}

// Login validates user password and returns identity info with a signed JWT that expires in 30 minutes (default)
func (s *UserService) Login(e string, p string) (dtos.UserInfoDto, error) {
	user, err := s.FindByEmail(e)
	if err != nil {
		return dtos.UserInfoDto{}, err
	}

	isValid := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(p))
	if isValid != nil {
		return dtos.UserInfoDto{}, isValid
	}

	user, err = s.FindByEmail(e)
	if err != nil {
		return dtos.UserInfoDto{}, err
	}

	return s.AuthService.signJWT(user)
}

// Create creates an active user with the given name, email, and password.
func (s *UserService) Create(name, email, password string) error {
	_, err := s.FindByEmail(email)
	if err == nil {
		return errs.New("User with that email already exists.")
	}
	return s.create(name, email, password, true, false)
}

// CreatePending creates a non-active user with the given name, email, and password.
func (s *UserService) CreatePending(name, email, password string, reset bool) error {
	_, err := s.FindByEmail(email)
	if err == nil {
		return errs.New("User with that email already exists.")
	} else if err.Error() == "sql: no rows in result set" {
		return s.create(name, email, password, false, reset)
	}
	return err
}

// Activate activates a pending user with the specified UUID and activation code.
func (s *UserService) Activate(code string, uid uuid.UUID) error {
	user, err := s.Find(uid)
	if err != nil {
		return err
	}

	if user.Active {
		return errs.New("User already activated")
	}

	pendingUser, err := s.FindPending(uid)
	if err != nil {
		return err
	}

	if pendingUser.Code == code {

		t, err := s.DB.Begin()
		if err != nil {
			return err
		}

		{
			var query = "UPDATE users SET active = true WHERE uid = ?"
			stmt, err := t.Prepare(query)
			if err != nil {
				t.Rollback()
				return err
			}
			defer stmt.Close()

			_, err = stmt.Exec(uid.String())
			if err != nil {
				t.Rollback()
				return err
			}
		}

		{
			var query = "DELETE FROM pending_users WHERE user_uid = ?"
			stmt, err := t.Prepare(query)
			if err != nil {
				t.Rollback()
				return err
			}
			defer stmt.Close()

			_, err = stmt.Exec(uid.String())
			if err != nil {
				t.Rollback()
				return err
			}
		}

		err = t.Commit()
		if err != nil {
			t.Rollback()
			return err
		}
		log.Printf("User %v activated successfully\n", uid.String())
		return nil
	}
	return errs.New("Invalid activation code.")
}

// SetPassword sets a new password for a user if the old password provided matches the current one.
func (s *UserService) SetPassword(uid uuid.UUID, old, new string) error {
	user, err := s.Find(uid)
	if err != nil {
		return fmt.Errorf("user does not exist with specified uuid: %v", uid)
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(old))
	if err == nil {
		hashedAndSalted, err := bcrypt.GenerateFromPassword([]byte(new), BcrpytCost)
		if err != nil {
			return errs.Wrap(err, "failed generating secure password.")
		}

		t, err := s.DB.Begin()
		if err != nil {
			return err
		}
		var query = "UPDATE users SET password = ?, reset_password = ? WHERE uid = ?"
		stmt, err := t.Prepare(query)
		if err != nil {
			t.Rollback()
			return err
		}
		defer stmt.Close()

		_, err = stmt.Exec(hashedAndSalted, false, uid)
		if err != nil {
			t.Rollback()
			return err
		}
		err = t.Commit()
		if err != nil {
			t.Rollback()
			return err
		}

	}
	return err
}

// ResetPassword sets a new password for a user, bypassing the old password check.
func (s *UserService) ResetPassword(uid uuid.UUID, new string) error {
	_, err := s.Find(uid)
	if err != nil {
		return fmt.Errorf("user does not exist with specified uuid: %v", uid)
	}

	hashedAndSalted, err := bcrypt.GenerateFromPassword([]byte(new), BcrpytCost)
	if err != nil {
		return errs.Wrap(err, "failed generating secure password.")
	}

	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "UPDATE users SET password = ?, reset_password = ? WHERE uid = ?"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(hashedAndSalted, false, uid)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	return nil
}

// FindByEmail finds a user by their email address.
func (s *UserService) FindByEmail(e string) (models.User, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM users WHERE email = ?")
	if err != nil {
		return models.User{}, err
	}
	defer stmt.Close()
	var user models.User
	err = stmt.QueryRow(e).Scan(&user.UID, &user.Name, &user.Password, &user.Email, &user.Active, &user.ResetPassword, &user.CreatedOn, &user.UpdatedOn, &user.CreatedBy, &user.UpdatedBy)
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

// Find retrieves a user by their UUID.
func (s *UserService) Find(uid uuid.UUID) (models.User, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM users WHERE uid = ?")
	if err != nil {
		return models.User{}, err
	}
	defer stmt.Close()
	var user models.User
	err = stmt.QueryRow(uid.String()).Scan(&user.UID, &user.Name, &user.Password, &user.Email, &user.Active, &user.ResetPassword, &user.CreatedOn, &user.UpdatedOn, &user.CreatedBy, &user.UpdatedBy)
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

// FindPending retrieves a pending user by their UUID.
func (s *UserService) FindPending(uid uuid.UUID) (models.PendingUser, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM pending_users WHERE user_uid = ?")
	if err != nil {
		return models.PendingUser{}, err
	}
	defer stmt.Close()
	var user models.PendingUser
	err = stmt.QueryRow(uid.String()).Scan(&user.ID, &user.UserUID, &user.Code, &user.CreatedOn, &user.UpdatedOn, &user.CreatedBy, &user.UpdatedBy)
	if err != nil {
		return models.PendingUser{}, err
	}
	return user, nil
}

// List retrieves all users.
func (s *UserService) List() ([]models.User, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM users")
	if err != nil {
		return make([]models.User, 0), err
	}
	defer stmt.Close()
	var users []models.User
	rows, err := stmt.Query()
	if err != nil {
		return make([]models.User, 0), err
	}
	for rows.Next() {
		var user models.User
		err = rows.Scan(&user.UID, &user.Name, &user.Password, &user.Email, &user.Active, &user.ResetPassword, &user.CreatedOn, &user.UpdatedOn, &user.CreatedBy, &user.UpdatedBy)
		if err != nil {
			return make([]models.User, 0), err
		}
		users = append(users, user)
	}
	return users, nil
}

// create adds a new user to the database with the given attributes.
func (s *UserService) create(name, email, password string, active, reset bool) error {
	var hashedAndSalted []byte
	var err error
	if reset {
		password = gotils.RandomString(12)
	}

	hashedAndSalted, err = bcrypt.GenerateFromPassword([]byte(password), BcrpytCost)
	if err != nil {
		return errs.Wrap(err, "failed generating secure password.")
	}

	t, err := s.DB.Begin()
	if err != nil {
		return err
	}

	uid := uuid.NewV4()
	{
		var query = "INSERT INTO users (uid, name, email, password, active, reset_password) VALUES (?,?,?,?,?,?)"
		stmt, err := t.Prepare(query)
		if err != nil {
			t.Rollback()
			return err
		}
		defer stmt.Close()

		_, err = stmt.Exec(uid, name, email, hashedAndSalted, active, reset)
		if err != nil {
			t.Rollback()
			return err
		}
	}

	hasher := md5.New()
	_, err = hasher.Write([]byte(uid.String()))
	if err != nil {
		return err
	}
	activationCode := hex.EncodeToString(hasher.Sum(nil))[:8]

	if !active {
		var query = "INSERT INTO pending_users (user_uid, code) VALUES (?,?)"
		stmt, err := t.Prepare(query)
		if err != nil {
			t.Rollback()
			return err
		}
		defer stmt.Close()
		_, err = stmt.Exec(uid.String(), activationCode)
		if err != nil {
			t.Rollback()
			return err
		}
	}

	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}

	log.Printf("User was created: %v\n", uid)
	if !active {
		s.sendActivationEmail(reset, uid, email, password, activationCode)
	}
	return nil
}

// sendActivationEmail sends an activation email to the user with the given attributes.
func (s *UserService) sendActivationEmail(reset bool, uid uuid.UUID, email, password, activationCode string) error {
	if s.EmailService.Config == nil {
		return errs.Errorf("User needs to be activated, but email service does not exist")
	}

	var secure = "http://"
	if os.Getenv("ENV") != "" {
		secure = "https://"
	}

	activataionURL := fmt.Sprintf("%v%v/#/activate/%v/%v", secure, s.AppDomain, uid.String(), activationCode)
	templateVars := map[string]string{
		"code":          activationCode,
		"activationUrl": activataionURL,
	}

	if reset {
		templateVars["password"] = password
	}

	err := s.EmailService.SendTemplated("confirm_email", email, "Confirm your Email", templateVars)
	if err != nil {
		return err
	}
	return nil
}
