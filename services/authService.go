package services

import (
	"crypto/rsa"
	"errors"
	"log"
	"net/http"
	"time"

	jose "github.com/go-jose/go-jose/v3"
	"github.com/go-jose/go-jose/v3/jwt"
	errs "github.com/pkg/errors"
	"gitlab.com/connorbrez/go-identity-server/dtos"
	"gitlab.com/connorbrez/go-identity-server/models"
	"gitlab.com/connorbrez/gotils"
)

// AuthService provides methods for authentication and authorization.
type AuthService struct {
	PrivateKey  *rsa.PrivateKey
	Jwt         map[string]string
}

// ValidateBearer validates the JWT token from the Authorization header of an HTTP request.
// It returns the token claims if the validation is successful, otherwise an error.
func (s *AuthService) ValidateBearer(r *http.Request) (jwt.Claims, error) {
	token, err := gotils.GetAuthHeader(r)
	if err != nil {
		log.Println("AuthService/ValidateBearer: error getting header")
		return jwt.Claims{}, err
	}

	claims, err := s.ValidateJWT(token)
	if err != nil {
		log.Println("AuthService/ValidateBearer: could not validate jwt")
		return jwt.Claims{}, err
	}

	return claims, nil
}

// ValidateJWT validates the corresponding JWT (t) with the public key and returns the claims.
// It verifies the token's signature and checks if the token has expired.
func (s *AuthService) ValidateJWT(t string) (jwt.Claims, error) {
	parsedJWT, err := jwt.ParseSigned(t)
	if err != nil {
		return jwt.Claims{}, errs.Wrap(err, "failed parsing JWT")
	}

	resultCl := jwt.Claims{}
	err = parsedJWT.Claims(&s.PrivateKey.PublicKey, &resultCl)
	if err != nil {
		return jwt.Claims{}, errs.Wrap(err, "couldn't retreive claims")
	}

	if time.Now().After(resultCl.Expiry.Time()) {
		return jwt.Claims{}, errors.New("jwt expired")
	}

	return resultCl, nil
}

// signJWT signs a JWT for the provided models.User (u) and returns the user info along with the token.
// It creates a signer using the RSA private key and the jose package, defines JWT claims, and signs the JWT.
func (s *AuthService) signJWT(u models.User) (dtos.UserInfoDto, error) {
	key := jose.SigningKey{Algorithm: jose.RS512, Key: s.PrivateKey}

	var signerOpts = jose.SignerOptions{}
	signerOpts.WithType("JWT")

	rsaSigner, err := jose.NewSigner(key, &signerOpts)
	if err != nil {
		log.Fatalf("failed to create signer: %+v\n", err)
		return dtos.UserInfoDto{}, err
	}

	cl := jwt.Claims{
		Subject:   u.UID.String(),
		Issuer:    s.Jwt["issuer"],
		NotBefore: jwt.NewNumericDate(time.Now()),
		Expiry:    jwt.NewNumericDate(time.Now().Add(time.Duration(30 * time.Minute))),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		Audience:  jwt.Audience{s.Jwt["audience"]},
	}

	raw, err := jwt.Signed(rsaSigner).Claims(cl).CompactSerialize()
	if err != nil {
		log.Fatalf("failed to sign with rsa signer, %v\n", err)
		return dtos.UserInfoDto{}, err
	}

	userInfo := u.ToUserInfo()
	userInfo.Token = raw
	return userInfo, nil
}
