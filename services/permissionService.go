package services

import (
	"database/sql"
	"log"

	"gitlab.com/connorbrez/go-identity-server/models"
)

// PermissionService provides methods for managing permissions in the database.

type PermissionService struct {
	DB *sql.DB // Database connection.
}

// Create adds a new permission with the given name and description to the database.
// It wraps the operation in a transaction to ensure consistency.
func (s *PermissionService) Create(name, description string) error {
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "INSERT INTO permissions (name, description) VALUES (?,?)"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(description, name)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("permission %v was created successfuly.\n", name)
	return nil
}

// Find retrieves a permission by its ID.
// It returns the permission or an error if not found.
func (s *PermissionService) Find(id int) (models.Permission, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM permissions WHERE id = ?")
	if err != nil {
		return models.Permission{}, err
	}
	defer stmt.Close()
	var permission models.Permission
	err = stmt.QueryRow(id).Scan(&permission.ID, &permission.Name, &permission.Description, &permission.CreatedOn, &permission.UpdatedOn, &permission.CreatedBy, &permission.UpdatedBy)
	if err != nil {
		return models.Permission{}, err
	}
	return permission, nil
}

// List retrieves all permissions from the database.
// It returns a slice of permissions or an error if the query fails.
func (s *PermissionService) List() ([]models.Permission, error) {
	stmt, err := s.DB.Prepare("SELECT * FROM permissions")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	var permissions []models.Permission
	rows, err := stmt.Query()
	for rows.Next() {
		var permission models.Permission
		rows.Scan(&permission.ID, &permission.Name, &permission.Description, &permission.CreatedOn, &permission.UpdatedOn, &permission.CreatedBy, &permission.UpdatedBy)
		if err != nil {
			return nil, err
		}
		permissions = append(permissions, permission)
	}

	return permissions, nil
}

// ListByRole retrieves all permissions assigned to the specified role from the database.
// It returns a slice of permissions or an error if the query fails.
func (s *PermissionService) ListByRole(roleId int) ([]models.Permission, error) {
	query := `SELECT role_permissions.permission_id as id,
                     permissions.name,
                     permissions.description,
                     permissions.created_on,
                     permissions.created_by,
                     permissions.updated_by,
                     permissions.updated_on
              FROM role_permissions
              INNER JOIN permissions ON role_permissions.permission_id = permissions.id
              WHERE role_permissions.role_id = ?`


	stmt, err := s.DB.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	var permissions []models.Permission
	rows, err := stmt.Query(roleId)
	for rows.Next() {
		var permission models.Permission
		rows.Scan(&permission.ID, &permission.Name, &permission.Description, &permission.CreatedOn, &permission.UpdatedOn, &permission.CreatedBy, &permission.UpdatedBy)
		if err != nil {
			return nil, err
		}
		permissions = append(permissions, permission)
	}

	return permissions, nil
}

// Delete removes a permission from the database by its ID.
// It wraps the operation in a transaction to ensure consistency.
func (s *PermissionService) Delete(id int) error {
	t, err := s.DB.Begin()
	if err != nil {
		return err
	}
	var query = "DELETE FROM permissions WHERE id = ?"
	stmt, err := t.Prepare(query)
	if err != nil {
		t.Rollback()
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id)
	if err != nil {
		t.Rollback()
		return err
	}
	err = t.Commit()
	if err != nil {
		t.Rollback()
		return err
	}
	log.Printf("permission %v was deleted successfully.\n", id)
	return nil
}
