package services

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/mailgun/mailgun-go/v3"
)

type EmailService struct {
	Config map[string]string
}

// SendTemplated will send the specified email template from mailgun with the specified to, subject and template variables
func (s *EmailService) SendTemplated(templateName, to, subject string, templateVars map[string]string) error {
	log.Println(s.Config["domain"])
	mg := mailgun.NewMailgun(s.Config["domain"], s.Config["key"])
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	m := mg.NewMessage(s.Config["from"], subject, "", to)
	m.SetTemplate(templateName)

	vars, err := json.Marshal(templateVars)
	if err != nil {
		return err
	}
	m.AddHeader("X-Mailgun-Variables", string(vars))

	_, _, err = mg.Send(ctx, m)
	return err
}
