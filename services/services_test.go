package services

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"log"
    "log/slog"
    "os"
	"strings"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/connorbrez/go-identity-server/enums"
	"gitlab.com/connorbrez/go-identity-server/models"
	"gitlab.com/connorbrez/go-identity-server/util"
)

var roleService RoleService
var permissionService PermissionService
var authService AuthService
var userService UserService

var user models.User

func TestMain(m *testing.M) {
	env := os.Getenv("ENV")
	if env == "" {
		env = "testing_local"
	}

	slog.Info(os.Getwd())

	log.Printf("Running in %s mode... \n", env)

	config := util.LoadConfig(env)
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		log.Fatalf("Failed to load private key from path...\n%s\n", err)
	}

	if strings.HasPrefix("testing", env) {
		config.Database["pass"] = os.Getenv("DB_TEST_PASS")
	}
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", config.Database["user"], config.Database["pass"], config.Database["host"], config.Database["port"], config.Database["database"])
	db := util.InitDB(config.Database["type"], dsn)

	err = util.DoMigrations(config.Database["type"],"../migrations/", db)
	if err != nil {
		log.Println(err)
	}

	err = util.DoMigrationsTest(config.Database["type"],"../migrations_test/", db)
	if err != nil {
		log.Println(err)
	}

	roleService = RoleService{DB: db}
	permissionService = PermissionService{DB: db}
	authService = AuthService{PrivateKey: privateKey, Jwt: config.Jwt}
	userService = UserService{DB: db, AppDomain: config.AppDomain, AuthService: &authService}

	user, err = userService.FindByEmail("testing@test.com")
	if err != nil {
		log.Println(err)
	}

	os.Exit(m.Run())
}

func TestSignAndValidateJwt(t *testing.T) {
	signedUser, err := authService.signJWT(user)

	if err != nil {
		t.Fatal(err)
	}

	if signedUser.UID != user.UID {
		t.Fatalf("UID property does not match. It should be: %v. It is: %v \n", user.UID, signedUser.UID)
	}

	if signedUser.Name != user.Name {
		t.Fatalf("Name property does not match. It should be: %v. It is: %v \n", user.Name, signedUser.Name)
	}

	if signedUser.Email != user.Email {
		t.Fatalf("Email property does not match. It should be: %v. It is: %v \n", user.Email, signedUser.Email)
	}

	if signedUser.Active != user.Active {
		t.Fatalf("Active property does not match. It should be: %v. It is: %v \n", user.Active, signedUser.Active)
	}

	if signedUser.ResetPassword != user.ResetPassword {
		t.Fatalf("ResetPassword property does not match. It should be: %v. It is: %v \n", user.ResetPassword, signedUser.ResetPassword)
	}

	splitToken := strings.Split(signedUser.Token, ".")

	if len(splitToken) != 3 {
		t.Fatalf("Token property does has invalid format... Must follow JWT format (xyz.xyz.xyz)... see https://jwt.io for more info")
	}

	claims, err := authService.ValidateJWT(signedUser.Token)
	if err != nil {
		t.Fatal(err)
	}

	if claims.Subject != signedUser.UID.String() {
		t.Fatalf("Subject does not match. It should be: %v. It is: %v \n", signedUser.UID, claims.Subject)
	}
}

func TestIsAuthorized(t *testing.T) {
	log.Println(user.UID)
	// user should have deafult permissions, test to see if they have one
	permId := int(enums.CreatePermission)
	result, _ := userService.HasPermission(user.UID.String(), permId)

	if !result {
		t.Fatalf("User should have permission with id: %v", permId)

	}

	// test to see if user has non existent permission
	permId = 999999
	result, _ = userService.HasPermission(user.UID.String(), permId)

	if result {
		t.Fatalf("User should not have permission with id: %v", permId)

	}

}
