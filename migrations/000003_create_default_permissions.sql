-- +goose Up

INSERT INTO permissions SET name = 'create_permission', description = 'Ability to create permissions';
INSERT INTO permissions SET name = 'delete_permission', description = 'Ability to delete permissions';
INSERT INTO permissions SET name = 'read_permission', description = 'Ability to read permissions';

INSERT INTO permissions SET name = 'read_user', description = 'Ability to read users';
INSERT INTO permissions SET name = 'delete_user', description = 'Ability to delete users';

INSERT INTO permissions SET name = 'create_role', description = 'Ability to create roles';
INSERT INTO permissions SET name = 'delete_role', description = 'Ability to delete roles';
INSERT INTO permissions SET name = 'read_role', description = 'Ability to read roles';

INSERT INTO permissions SET name = 'grant_permission', description = 'Ability to grant permissions to roles';
INSERT INTO permissions SET name = 'revoke_permission', description = 'Ability to revoke permissions from roles';

INSERT INTO permissions SET name = 'grant_role', description = 'Ability to grant roles to users';
INSERT INTO permissions SET name = 'revoke_role', description = 'Ability to revoke roles from users';

INSERT INTO roles SET name = 'admin', description = 'Admin Role';

INSERT INTO role_permissions SET permission_id = '1', role_id = '1';
INSERT INTO role_permissions SET permission_id = '2', role_id = '1';
INSERT INTO role_permissions SET permission_id = '3', role_id = '1';
INSERT INTO role_permissions SET permission_id = '4', role_id = '1';
INSERT INTO role_permissions SET permission_id = '5', role_id = '1';
INSERT INTO role_permissions SET permission_id = '6', role_id = '1';
INSERT INTO role_permissions SET permission_id = '7', role_id = '1';
INSERT INTO role_permissions SET permission_id = '8', role_id = '1';
INSERT INTO role_permissions SET permission_id = '9', role_id = '1';
INSERT INTO role_permissions SET permission_id = '10', role_id = '1';
INSERT INTO role_permissions SET permission_id = '11', role_id = '1';
INSERT INTO role_permissions SET permission_id = '12', role_id = '1';

-- +goose Down

DELETE FROM role_permissions WHERE role_id = '1';

DELETE FROM permissions WHERE name = 'create_permission';
DELETE FROM permissions WHERE name = 'delete_permission';
DELETE FROM permissions WHERE name = 'read_permission';

DELETE FROM permissions WHERE name = 'create_role';
DELETE FROM permissions WHERE name = 'delete_role';
DELETE FROM permissions WHERE name = 'read_role';

DELETE FROM permissions WHERE name = 'grant_permission';
DELETE FROM permissions WHERE name = 'revoke_permission';

DELETE FROM permissions WHERE name = 'grant_role';
DELETE FROM permissions WHERE name = 'revoke_role';

DELETE FROM permissions WHERE name = 'delete_users';
DELETE FROM permissions WHERE name = 'read_users';

