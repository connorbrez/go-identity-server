-- +goose Up
CREATE TABLE IF NOT EXISTS pending_users (
    id integer PRIMARY KEY AUTO_INCREMENT,
    user_uid varchar(36) NOT NULL,
    code varchar(36) NOT NULL,
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by varchar(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
    updated_by varchar(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'
);


-- +goose Down
DROP TABLE pending_users;