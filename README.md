# A Go Identity Server

This is a hobby project of mine to teach myself more about writing go, authentication and authorization among other things. 

The idea is a basic authentication server using JWTs aswell as RBAC alongside that.


## Generating/Rolling RSA Private Key

`./rollkey.sh` - this will delete the old local rsa keys and create new ones (src/rsa_local & src/rsa_local.pub) 

`./rollkey.sh -s {env}` - this will delete the old rsa keys and create new ones with whatever suffix that has been provided. 


## Go Configuration Files

Configuration files for the backend live in `src/config`. The config is loaded base on an environment variable `ENV`. Default is `local`.


## Quick Local Database Setup

setting up a local database is super quick with the use of docker.
`sudo docker run --detach --name mariadev -p 3306:3306 --env MARIADB_USER=localUser --env MARIADB_PASSWORD=pass123 --env MARIADB_ROOT_PASSWORD=root123  mariadb:latest`

You will need to create a database with the same name in the config, which is specified in `src/config`. Default is `go_rbac`.


## Roadmap

    * Roll Server private keys periodically
    * Admin Dashboard
    * Automated deployments to:
      - GCP
      - Azure
      - Digital Ocean
      - AWS
